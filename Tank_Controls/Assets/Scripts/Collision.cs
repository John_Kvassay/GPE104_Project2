﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour {

	void OnCollisionEnter2D (Collision2D other)
	// When using OnCollisionEnter2D, Set the parameter to Collsion2D

	{
		Destroy(other.gameObject);
		Destroy(gameObject);
	}
}
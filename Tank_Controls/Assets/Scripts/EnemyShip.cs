﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShip : MonoBehaviour {
    public Vector3 direction;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		//if (gameManager.instance == null) {
			int m = 0;
		//}		
		//if (gameManager.instance.player == null) {
			//int m = 0;
		//}
        direction = gameManager.instance.player.transform.position - transform.position;
        direction.Normalize();
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

        Quaternion targetRotation = Quaternion.Euler(0, 0, angle);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, gameManager.instance.enemyRotationSpeed * Time.deltaTime);

        transform.Translate(Time.deltaTime * gameManager.instance.enemyShipSpeed, 0, 0);

		
	}
}

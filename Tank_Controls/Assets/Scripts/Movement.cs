﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {


	private Transform tf;
	public float turnspeedLeft; // Make a negative number to signify movement in the oposite direction
	public float turnspeedRight; // Make a positve number
	public float speed; 



	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform>(); // Loads the game object transform into the variable
	}

	// Update is called once per frame
	void Update () {

		// Turns the object left 
		// Uses a negative number to indicate rotation in the oposite direction
		if (Input.GetKey(KeyCode.LeftArrow))
		{
			tf.Rotate(0, 0, turnspeedLeft);
		}

		// Turns the object right 
		if (Input.GetKey(KeyCode.RightArrow))
		{
			tf.Rotate(0, 0, turnspeedRight);
		}

		// Makes the object go forward
		if (Input.GetKey(KeyCode.UpArrow))
		{
			tf.position += (tf.rotation * Vector3.right) * speed; // use rotation in order to get the objects rotation

		}


		if (Input.GetKeyDown (KeyCode.Space)) {
			fire ();
		}

	}


	void fire(){
		GameObject projectile = Instantiate<GameObject> (gameManager.instance.projectile, gameManager.instance.projectileSpawn.position, gameManager.instance.projectileSpawn.rotation);
	}
}

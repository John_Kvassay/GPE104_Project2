﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Destroy (this.gameObject, gameManager.instance.projectileLife);
	}

	// Update is called once per frame
	void Update () {
		transform.position += (transform.right * (Time.deltaTime * gameManager.instance.projectileSpeed)); 
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameManager : MonoBehaviour {

    // Singleton
	// gameManager instance is the singleton
    public static gameManager instance; // Makes the variables in the GameManager accessable to other scripts

        public int playerLives;
        public GameObject player;

        // Astroids 
        public GameObject[] asteroidSpawnPoints;
	    public List<GameObject> astroids;
        public float astroidSpeed;
        
        // Enemy Ship
        public float enemyShipSpeed;
        public float enemyRotationSpeed;

		// Projectile
		public GameObject projectile;
		public Transform projectileSpawn; 
		public float projectileSpeed;
		public float projectileLife;

    // Enemies
    public List<GameObject> enemiesAlive;
        public bool removingEnemies;
        public int maximumActiveEnemies;
        
        

    
    // Creates the GameManager instance
	void Awake()
	{
		if (instance == null) {
			instance = this; // Sets instance = to gameManager
			DontDestroyOnLoad (gameObject);
		} else {
			Destroy (gameObject);
		}
	}





	// Use this for initialization
	void Start () {
        if (enemiesAlive.Count < maximumActiveEnemies)
        {
            // Determine spawn point
            int id = Random.Range(0, asteroidSpawnPoints.Length);
            GameObject point = asteroidSpawnPoints[id];

            // Determine which asteroid to spawn
            GameObject astroid = astroids[Random.Range(0, astroids.Count)];

            // Instantiate an asteroid
            GameObject astroidInstance = Instantiate<GameObject>(astroid, point.transform.position, Quaternion.identity);

            /*
            Vector2 directionVector = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
            directionVector.Normalize();
            astroidInstance.GetComponent<Astroid>().direction = directionVector;
            */
            // Add to enemies list
            enemiesAlive.Add(astroidInstance);
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
